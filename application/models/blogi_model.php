<?php


class blogi_model extends CI_Model{
    public function __construct() {
        parent::__construct();
    }
 
    
    public function hae_blogikirjoitukset() {
        $this->db->select('*');
        $this->db->from('kayttaja');
        $this->db->join('kirjoitus', 'kayttaja.id = kirjoitus.kayttaja_id');
        $sql_blogi=$this->db->get();
        return $sql_blogi->result();
    }
    
    public function hae_blogikirjoitus($id) {
        $this->db->select('kirjoitus.id As id, tunnus, kirjoitus.teksti AS teksti, kommentti.teksti AS kommentti, kirjoitus.paivays, kirjoitus.otsikko');
        $this->db->from('kayttaja');
        $this->db->where('kirjoitus.id', $id);
        $this->db->join('kirjoitus', 'kayttaja.id = kirjoitus.kayttaja_id');
        $this->db->join('kommentti', 'kirjoitus.id = kommentti.id');
        $sql_blogi=$this->db->get();
        return $sql_blogi->result();
    }
    
    public function text_insert($data) {
        $this->db->insert('kirjoitus', $data);
    }

        

}

