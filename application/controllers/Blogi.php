<?php



class Blogi extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('blogi_model');
    }
    
    public function index(){

       $data['blogi']= $this->blogi_model->hae_blogikirjoitukset();
        $data['main_content']= 'blogi/blogi_view';
        $this->load->view('template',$data);
    }
    
    public function teksti($id) {
//        $id=3;
        $data['blogi']= $this->blogi_model->hae_blogikirjoitus($id);
        $data['main_content']= 'blogi/teksti_view';
        $this->load->view('template',$data);
    }
    
    public function kirjaudu() {
        $data['main_content']="blogi/kirjaudu_view";
        $this->load->view("template",$data);
    }
    
    public function rekisteroidy() {
        $data['main_content']="blogi/rekisteroityminen_view";
        $this->load->view("template",$data);
    }
    
    
    
    public function kirjoita() {
        $data['main_content']="blogi/kirjoita_view";
        $this->load->view("template",$data);
    }
    
    public function julkaise() {
        $data = array(
'otsikko' => $this->input->post('otsikko'),
'teksti' => $this->input->post('teksti'),
            'kayttaja_id' => $_SESSION['kayttaja_id']
);
$result = $this->blogi_model->text_insert($data);
if ($result == TRUE) {
$data['message_display'] = 'Registration Successfull !';
redirect();
} else {
$data['message_display'] = 'Username already exist!';
redirect();
    }
    
}
    
        
    

}
