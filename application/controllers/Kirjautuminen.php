<?php



class Kirjautuminen extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('blogi_model');
        $this->load->model('login_database');
    }
    
    public function new_user_registration() {


$data = array(
'tunnus' => $this->input->post('username'),
'email' => $this->input->post('email'),
'etunimi' => $this->input->post('first_name'),
'sukunimi' => $this->input->post('last_name'),
'salasana' => md5($this->input->post('password'))
);
$result = $this->login_database->registration_insert($data);
if ($result == TRUE) {
$data['message_display'] = 'Registration Successfull !';
redirect();
} else {
$data['message_display'] = 'Username already exist!';
redirect();

}
}

// Check for user login process
public function user_login_process() {


$data = array(
'username' => $this->input->post('username'),
'password' => $this->input->post('password')
);
$result = $this->login_database->login($data);
if ($result == TRUE) {

$username = $this->input->post('username');
$result = $this->login_database->read_user_information($username);
if ($result != false) {

// Add user data in session
    $data =[
        'kayttaja_id' => $result[0]->id,
        'logged_in' => TRUE
    ];
$this->session->set_userdata($data);
redirect();
}
} else {
$data = array(
'error_message' => 'Invalid Username or Password'
);
$data['main_content']="blogi/kirjaudu_view";
        $this->load->view("template",$data);
}
}
//}

// Logout from admin page
public function logout() {

// Removing session data
$sess_array = array(
'username' => ''
);
$this->session->unset_userdata('logged_in', $sess_array);
$data['message_display'] = 'Successfully Logout';
$data['main_content']="blogi/kirjaudu_view";
        $this->load->view("template",$data);
}

}

