<div class="container">

      <div class="starter-template">
        <h1>Lisää kirjoitus</h1>
        <?php echo form_open('blogi/julkaise'); ?>
            <div class="form-group">
                <label for="otsikko">Otsikko:</label>
                <input type="text" class="form-control" id="otsikko" name="otsikko" required>
            </div>
            <div class="form-group">
                <label for="teksti">Teksti:</label>
                <textarea name="teksti" rows="5" cols="30" required class="form-control"></textarea><br>
            </div>
            <button type="submit" class="btn btn-default">Tallenna</button>
<!--            <button class="btn btn-default" onclick="location.href='index.php';">Peruuta</button>-->
        <?php echo form_close(); ?>
      </div>

    </div><!-- /.container -->
<?php



