<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Blogi</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link <?php print 'href=' . base_url() . 'css/style.css' ?> rel="stylesheet">
        
    </head>
    <body>
        <?php 
        
//        session_start() 
        ?>  
        
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php print(site_url()); ?>/blogi/index">Blogi</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
              <li><a href="<?php print(site_url()); ?>/blogi/index">Etusivu</a></li>
            <?php if(isset($_SESSION['logged_in'])){ ?>
            <li><a href="<?php print(site_url()); ?>/blogi/kirjoita">Lisää kirjoitus</a></li>
            <li><a href="<?php print(site_url()); ?>/kirjautuminen/logout">Kirjaudu ulos</a></li>
            <?php } else { ?>
            <li><a href="<?php print(site_url()); ?>/blogi/kirjaudu">Kirjaudu</a></li>
            <li><a href="<?php print(site_url()); ?>/blogi/rekisteroidy">Rekisteröidy</a></li>
            <?php } ?>
            
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
        <?php
            $this->load->view($main_content);
        ?>
        <script <?php print 'src=' . base_url() . 'js/asiakas.js'?>></script>
        
        <script
  src="http://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </body>
</html>
